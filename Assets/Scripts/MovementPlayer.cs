﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour
{
    [SerializeField]
    private float characterSpeed = 1;
    [SerializeField]
    private float jumpForce = 400;
    public GameObject platformDetection;
    private FloorDetectionHandler floorDetection;
    private Transform playerPos;
    private SpriteRenderer spriteRenderer;
    private Animator animation;
    private Vector3 startJump;
    void Start()
    {
        animation = this.gameObject.GetComponent<Animator>();
        spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        playerPos = this.gameObject.transform;
        floorDetection = platformDetection.GetComponent<FloorDetectionHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Floor: "+floorDetection.floorDetect+"\tRunning: "+animation.GetBool("running")+ ", JumpStart: " + animation.GetBool("jumpStart")+ ", airTime: " + animation.GetBool("airTime"));
        if (Input.GetKey(KeyCode.RightArrow))
        {
            animation.SetBool("running", true);
            if (spriteRenderer.flipX)
            {
                spriteRenderer.flipX = false;
            }
            playerPos.position += Time.deltaTime * characterSpeed * Vector3.right;
        }
        else if(Input.GetKey(KeyCode.LeftArrow))
        {
            animation.SetBool("running", true);
            if (!spriteRenderer.flipX)
            {
                spriteRenderer.flipX = true;
            }
            playerPos.position += Time.deltaTime * characterSpeed * Vector3.left;
        }
        else
        {
            animation.SetBool("running", false);
        }
        if (Input.GetKeyDown(KeyCode.Space) && floorDetection.floorDetect)
        {
            startJump = playerPos.position;
            JumpHandle();
        }
        if (floorDetection.floorDetect)
        {
            animation.SetBool("jumpStart", false);
            animation.SetBool("airTime", false);
        }
        else
        {
            animation.SetBool("jumpStart", true);
            animation.SetBool("airTime", true);
        }
    }
    private void JumpHandle()
    {        
        this.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector3.up * jumpForce);        
    }
}
