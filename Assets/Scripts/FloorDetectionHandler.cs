﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDetectionHandler : MonoBehaviour
{
    public bool floorDetect
    {
        get; set;
    }

    private void Start()
    {
        floorDetect = true;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.CompareTag("Platform"))
        {
            floorDetect = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        floorDetect = false;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            floorDetect = true;
        }
    }
}
