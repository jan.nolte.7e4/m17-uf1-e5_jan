﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    public GameOverScreen gameOverScreen;
    private static int playerHP;
    private static GameManager _instance;
    private UIScript UI;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    private static int _score;
    public static int Score
    {
        get
        {
            return _score;
        }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        //DontDestroyOnLoad(this);
    }
    void Start()
    {
        UI = GameObject.Find("UI").GetComponent<UIScript>();
        if (playerHP <= 0)
        {
            playerHP = player.GetComponent<PlayerData>().maxlives;
        }        
    }
    void Update()
    {
        UI.Score.text = "Score:\t" + Score;
        UI.HP.text = "HP:\t" + playerHP;
    }
    public void GameOver()
    {
        gameOverScreen.Setup(Score);
    }
    public void restartScreen()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void loseHP(int? quantity)
    {
        Debug.Log(playerHP.ToString());
        if (quantity.HasValue)
        {
            playerHP -= quantity.Value;
        }
        else
        {
            playerHP--;
        }
        if (playerHP <= 0)
        {
            Destroy(player);
            GameOver();
            _score = 0;
            restartHP();
        }
        else
        {
            
            restartScreen();
            
        }
    }public void healPlayer(int? quantity)
    {
        if (quantity.HasValue)
        {
            playerHP += (playerHP<player.GetComponent<PlayerData>().maxlives)? quantity.Value:0;
        }
        else
        {
            if(playerHP< player.GetComponent<PlayerData>().maxlives) playerHP++;
        }
    }
    public void restartHP()
    {
        playerHP = player.GetComponent<PlayerData>().maxlives;
    }
    public void addScore(int? points)
    {
        if (points.HasValue)
        {
            _score += points.Value;
        }
        else
        {
            _score++;
        }
    }
    
}
