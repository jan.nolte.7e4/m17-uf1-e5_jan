﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public string CharacterName;
    public int maxlives=3;
    private float posActualY;
    private void Start()
    {
        posActualY = this.gameObject.GetComponent<Transform>().position.y;
    }
    private void Update()
    {
        if (this.gameObject.GetComponent<Transform>().position.y > posActualY+0.5f)
        {
            GameManager.Instance.addScore(5);
            posActualY = this.gameObject.GetComponent<Transform>().position.y + 0.5f;
        }
    }

}
