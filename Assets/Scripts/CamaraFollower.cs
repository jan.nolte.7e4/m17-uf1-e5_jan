﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollower : MonoBehaviour
{
    [SerializeField]
    private GameObject playerObject;
    private Transform playerPos;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        playerPos = playerObject.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (playerPos.position.y > transform.position.y + 0.5f)
        {
            // utilitzo SmoothDamp per suavitzar el moviment de la càmara
            Vector3 targetPosition = new Vector3(transform.position.x, playerPos.position.y + 0.5f, transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);            
        }
    }
}
